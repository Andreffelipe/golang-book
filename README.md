# [Materiais de Aprendizagem de Chinês Golang](http://go.wuhaolin.cn/)
Este livro resume e-books de Golang de código aberto de alta qualidade na Internet para que todos aprendam, abrangendo os três livros a seguir:

- [Guia de primeiros passos](the-way-to-go/README.md)
- [Go language Bible](gopl/README.md)
- [Go Web Programming](build-web-application-with-golang/README.md)

## Livros em papel recomendados

<h4><a href="https://u.jd.com/0dO22t" rel="noreferrer">Introdução à "Go Programming Language"</a></h4>
<a href="https://u.jd.com/0dO22t" rel="noreferrer">
    <img src="https://img1.360buyimg.com/n1/jfs/t5248/207/1621269134/210983/67ef6286/5912e2fcN787f6df5.jpg"/>
</a>

<h4><a href="https://u.jd.com/LsHsaY" rel="noreferrer">Combate real "Go Language Combat"</a></h4>
<a href="https://u.jd.com/LsHsaY" rel="noreferrer">
    <img src="https://img1.360buyimg.com/n1/jfs/t4120/142/1238030440/302452/3c514bbb/58be1c49N0069fd89.jpg"/>
</a>

<h4><a href="https://u.jd.com/UI8TrG" rel="noreferrer">Prática avançada de programação concorrente</a></h4>
<a href="https://u.jd.com/UI8TrG" rel="noreferrer">
    <img src="https://img1.360buyimg.com/n1/jfs/t5785/339/2011006819/38488/9e715cbd/592bf171Ne45f43a2.jpg"/>
</a>

## Aviso de direitos autorais
[Licença Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International](http://creativecommons.org/licenses/by-nc-sa/4.0/).
