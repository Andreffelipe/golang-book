# [Materiais de Aprendizagem de Chinês Golang](http://go.wuhaolin.cn/)
Este livro resume e-books de Golang de código aberto de alta qualidade na Internet para que todos aprendam, abrangendo os três livros a seguir:

- [Guia de primeiros passos](the-way-to-go/README.md)
- [Go language Bible](gopl/README.md)
- [Go Web Programming](build-web-application-with-golang/README.md)

## Livros em papel recomendados

<h4><a href="https://union-click.jd.com/jdc?e=&p=AyIGZRhZFwsQAlIfWxwyEgRVEl0dCxs3EUQDS10iXhBeGlcJDBkNXg9JHU4YDk5ER1xOGRNLGEEcVV8BXURFUFdfC0RVU1JRUy1OVxUBEg5TE1IcMmkAC3s%2FbHh7Yl1LLUt1VHwibz9rBXILWStaJQITBlQZXxMCFAVlK1sSMkBpja3tzaejG4Gx1MCKhTdUK1sRChUDVB1fFwQVD10rXBULIl4FRh1RMiI3VitrJQIiBGVZNRADFA9dSAtABxcOAh5cFgBCAQBIUhdXRlNTEwkdBEFXZRlaFAYb" rel="noreferrer">入门《Go程序设计语言》</a></h4 >
<a href="https://union-click.jd.com/jdc?e=&p=AyIGZRhZFwsQAlIfWxwyEgRVEl0dCxs3EUQDS10iXhBeGlcJDBkNXg9JHU4YDk5ER1xOGRNLGEEcVV8BXURFUFdfC0RVU1JRUy1OVxUBEg5TE1IcMmkAC3s%2FbHh7Yl1LLUt1VHwibz9rBXILWStaJQITBlQZXxMCFAVlK1sSMkBpja3tzaejG4Gx1MCKhTdUK1sRChUDVB1fFwQVD10rXBULIl4FRh1RMiI3VitrJQIiBGVZNRADFA9dSAtABxcOAh5cFgBCAQBIUhdXRlNTEwkdBEFXZRlaFAYb" rel="noreferrer">
    <img src="https://img1.360buyimg.com/n1/jfs/t5248/207/1621269134/210983/67ef6286/5912e2fcN787f6df5.jpg"/>
</a>

<h4><a href="https://union-click.jd.com/jdc?e=&p=AyIGZRhZFwsQAlIfWxwyEgRVGVwdBBc3EUQDS10iXhBeGlcJDBkNXg9JHU4YDk5ER1xOGRNLGEEcVV8BXURFUFdfC0RVU1JRUy1OVxUBEgVSE10QMnAdCU0pEWdPZz5LD3xleQczaDBFGEQLWStaJQITBlQZXxMCFAVlK1sSMkBpja3tzaejG4Gx1MCKhTdUK1sRChUDVB1fEgIbAlIrXBULIl4FRh1RMiI3VitrJQIiBGVZNUYAEwRQGVsTBUUEUR4IRQMbAgdMCxMCFARSTl9ABBADZRlaFAYb" rel="noreferrer">实战《Go语言实战》</a></h4>
<a href="https://union-click.jd.com/jdc?e=&p=AyIGZRhZFwsQAlIfWxwyEgRVGVwdBBc3EUQDS10iXhBeGlcJDBkNXg9JHU4YDk5ER1xOGRNLGEEcVV8BXURFUFdfC0RVU1JRUy1OVxUBEgVSE10QMnAdCU0pEWdPZz5LD3xleQczaDBFGEQLWStaJQITBlQZXxMCFAVlK1sSMkBpja3tzaejG4Gx1MCKhTdUK1sRChUDVB1fEgIbAlIrXBULIl4FRh1RMiI3VitrJQIiBGVZNUYAEwRQGVsTBUUEUR4IRQMbAgdMCxMCFARSTl9ABBADZRlaFAYb" rel="noreferrer">
    <img src="https://img1.360buyimg.com/n1/jfs/t4120/142/1238030440/302452/3c514bbb/58be1c49N0069fd89.jpg"/>
</a>

<h4><a href="https://union-click.jd.com/jdc?e=&p=AyIGZRhZFwsQAlIfWxwyEgRUHFkVBxI3EUQDS10iXhBeGlcJDBkNXg9JHU4YDk5ER1xOGRNLGEEcVV8BXURFUFdfC0RVU1JRUy1OVxUBEwBXG14VMnBvFEwzRhxJZRJbJ05FE3E2ZwYQUVQLWStaJQITBlQZXxMCFAVlK1sSMkBpja3tzaejG4Gx1MCKhTdUK1sRChUDVB1fHQIRAVUrXBULIl4FRh1RMiI3VitrJQIiBGVZNRILEQ9SElsWUhUDUh4OFAoaDlZMXUILG1VQTA4UVhMAZRlaFAYb" rel="noreferrer">进阶《Go并发编程实战》</a></h4>
<a href="https://union-click.jd.com/jdc?e=&p=AyIGZRhZFwsQAlIfWxwyEgRUHFkVBxI3EUQDS10iXhBeGlcJDBkNXg9JHU4YDk5ER1xOGRNLGEEcVV8BXURFUFdfC0RVU1JRUy1OVxUBEwBXG14VMnBvFEwzRhxJZRJbJ05FE3E2ZwYQUVQLWStaJQITBlQZXxMCFAVlK1sSMkBpja3tzaejG4Gx1MCKhTdUK1sRChUDVB1fHQIRAVUrXBULIl4FRh1RMiI3VitrJQIiBGVZNRILEQ9SElsWUhUDUh4OFAoaDlZMXUILG1VQTA4UVhMAZRlaFAYb" rel="noreferrer">
    <img src="https://img1.360buyimg.com/n1/jfs/t5785/339/2011006819/38488/9e715cbd/592bf171Ne45f43a2.jpg"/>
</a>

## Aviso de direitos autorais
[Licença Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International](http://creativecommons.org/licenses/by-nc-sa/4.0/).
